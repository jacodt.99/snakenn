//librerie esterne
var express = require("express");
var bodyParser = require('body-parser');




//file locali
var todoController = require("./controllers/controller");

//non modificare
var app = express();

app.use(bodyParser.json({limit: '50mb'}));

//setto ejs come view frameqwork da usare
app.set("view engine", "ejs");

//setto i css in tutte le pagine di /home
app.use("/css", express.static("css"));
app.use(express.static(__dirname));

//fire controllers
todoController(app);

//listen to port
app.listen(4000);
console.log("you are listening to port 4000");
