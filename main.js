import {
  muteBrain
} from './geneticAlgorithm.js';
import {
  createSnake
} from './snake.js';
import {
  setBrain
} from './neuralNetwork.js';

var getJSON = function (url, callback) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', url, true);
  xhr.responseType = 'json';
  xhr.onload = function () {
    var status = xhr.status;
    if (status === 200) {
      callback(null, xhr.response);
    } else {
      callback(status, xhr.response);
    }
  };
  xhr.send();
};


export function initialize() {

  getJSON('getFirstFive',
    function (err, data) {

      //console.log(data);
      if (err !== null) {} else {
        if (data.hasOwnProperty('FS')) {

          var brainData = muteBrain("first");
          setBrain(brainData);
          createSnake();
        } else {
          
          console.log(data);
          //console.log("inizializing");
          var brainData = muteBrain(data);
          setBrain(brainData);
          createSnake();
        }
      }
    });
  }


setInterval(() => {
  

  getJSON('getStats',
    function (err, data) {
      if (err !== null) {
        console.log("null");
      } else {
        //console.log(data);
        var sortedData = sortByProperty(data, 'generation', 1);

        var myNode = document.getElementById("statsTable");
        while (myNode.childNodes[2]) {
            myNode.removeChild(myNode.childNodes[2]);
        }


          for (var i = 0; i < sortedData.length; i++) {

            var tr = document.createElement("tr");
            document.getElementById("statsTable").appendChild(tr);
            tr.id = "gen" + i;

            var td = document.createElement("td");
            td.style.color = "whitesmoke";
            td.style.fontFamily = "'VT323', monospace";
            td.style.fontSize = "2em";
            td.textContent = sortedData[i].generation;
            document.getElementById("gen" + i).appendChild(td);

            var td = document.createElement("td");
            td.style.color = "whitesmoke";
            td.style.fontFamily = "'VT323', monospace";
            td.style.fontSize = "2em";
            td.textContent = sortedData[i].bestScore;
            document.getElementById("gen" + i).appendChild(td);

            var td = document.createElement("td");
            td.style.color = "whitesmoke";
            td.style.fontFamily = "'VT323', monospace";
            td.style.fontSize = "2em";
            td.textContent = (Math.round(sortedData[i].avgScore * 10) / 10);
            document.getElementById("gen" + i).appendChild(td);
          }
        
    }
  });
}, 5000);


initialize();




function sortByProperty(objArray, prop, direction) {
  if (arguments.length < 2) throw new Error("ARRAY, AND OBJECT PROPERTY MINIMUM ARGUMENTS, OPTIONAL DIRECTION");
  if (!Array.isArray(objArray)) throw new Error("FIRST ARGUMENT NOT AN ARRAY");
  const clone = objArray.slice(0);
  const direct = arguments.length > 2 ? arguments[2] : 1; //Default to ascending
  const propPath = (prop.constructor === Array) ? prop : prop.split(".");
  clone.sort(function (a, b) {
    for (let p in propPath) {
      if (a[propPath[p]] && b[propPath[p]]) {
        a = a[propPath[p]];
        b = b[propPath[p]];
      }
    }
    // convert numeric strings to integers
    return ((a < b) ? -1 * direct : ((a > b) ? 1 * direct : 0));
  });
  return clone;
}



function connectNode(div1, div2){

  var line = document.createElementNS('http://www.w3.org/2000/svg','line');
  line.id = "line" + div1.id + div2.id;
  document.getElementById("svg").appendChild(line);
  
  var x1 = div1.getBoundingClientRect().left + (div1.style.width/2) + 17;
  var y1 = div1.getBoundingClientRect().top + (div1.style.height/2) + 7.5;
  var x2 = div2.getBoundingClientRect().left + (div2.style.width/2);
  var y2 = div2.getBoundingClientRect().top + (div2.style.height/2) + 7.5;

  line.setAttribute('x1',x1);
  line.setAttribute('y1',y1);
  line.setAttribute('x2',x2);
  line.setAttribute('y2',y2);
}


function visualizeNN(){

  for (var i = 0; i < 24; i++){

    var neuron = document.createElement("div");
    neuron.className = "neuron layer0";
    neuron.id = "layer0" + "neuron" + i;
    document.getElementsByClassName("layer0")[0].appendChild(neuron);
  
  }
  for (var i = 1; i <= 4; i++){
  
    for (var j = 0; j < 8; j++){
      var neuron = document.createElement("div");
      neuron.className = "neuron layer" + i;
      neuron.id = "layer" + i + "neuron" + j;
      document.getElementsByClassName("layer"+i)[0].appendChild(neuron);
    }
  
  }
  
  for (var i = 0; i < 4; i++){
  
    var neuron = document.createElement("div");
    neuron.className = "neuron layer5";
    neuron.id = "layer5" + "neuron" + i;
    document.getElementsByClassName("layer5")[0].appendChild(neuron);
  }

  
  for (i = 0; i < 5; i++){

    document.getElementById("layer" + i).childNodes.forEach(firstLayerElement => {

      document.getElementById("layer" + (i + 1)).childNodes.forEach(secondLayerElement => {

        connectNode(firstLayerElement, secondLayerElement);
      });
    });
  }
}

visualizeNN();

