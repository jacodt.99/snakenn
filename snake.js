import {
    getNeuralNetwork
} from './neuralNetwork.js';
import {
    initialize
} from './main.js';
import {
    on
} from './configuration.js';
import {
    variables
} from './configuration.js';

class Snake {

    constructor(display, speed) {

        this.previousDirection = "top";
        this.speed = speed;
        this.display = display;
        this.position = [
            /*x y*/
            [17, 15], /*testa*/
            [17, 16],
            [17, 17], /*coda*/
            [17, 18], /*coda*/
            [17, 19] /*coda*/
        ]
        this.generateSnake();
    }

    generateSnake() {

    }

    goTop(direction) {

        //se non vado nella direzione opposta
        if (this.previousDirection != "bot") {

            //creo la nuova posizione al serpente
            var newPosition = [this.position[0][0], (this.position[0][1] - 1)];
            this.position.splice(0, 0, newPosition);
            //cancello la vecchia posizione
            this.position.splice(-1, 1);
            this.previousDirection = "top";

        } else this.goBot(direction);
        refreshDisplay();
    }
    goBot(direction) {

        //se non vado nella direzione opposta
        if (this.previousDirection != "top") {

            //creo la nuova posizione al serpente
            var newPosition = [this.position[0][0], (this.position[0][1] + 1)];
            this.position.splice(0, 0, newPosition);
            //cancello la vecchia posizione
            this.position.splice(-1, 1);
            this.previousDirection = "bot";
        } else this.goTop(direction);
        refreshDisplay();
    }
    goLeft() {

        //se non vado nella direzione opposta
        if (this.previousDirection != "right") {

            //creo la nuova posizione al serpente
            var newPosition = [this.position[0][0] - 1, (this.position[0][1])];
            this.position.splice(0, 0, newPosition);
            //cancello la vecchia posizione
            this.position.splice(-1, 1);
            this.previousDirection = "left";
        } else this.goRight(direction);
        refreshDisplay();
    }
    goRight() {

        //se non vado nella direzione opposta
        if (this.previousDirection != "left") {

            //creo la nuova posizione al serpente
            var newPosition = [this.position[0][0] + 1, (this.position[0][1])];
            this.position.splice(0, 0, newPosition);
            //cancello la vecchia posizione
            this.position.splice(-1, 1);
            this.previousDirection = "right";
        } else this.goLeft(direction);
        refreshDisplay();
    }
}

class Apple {

    constructor(display) {

        this.display = display;
        this.generateApple();
    }

    //genero una mela in una posizione random
    generateApple() {


        this.position = [
            (Math.floor(Math.random() * this.display.size)),
            (Math.floor(Math.random() * this.display.size))
        ];

        //controllo che la mela non sia nella stessa posizione del serpente
        for (var i = 0; i < this.display.snake.position.length; i++) {

            if (this.position[0] == this.display.snake.position[i][0] &&
                this.position[1] == this.display.snake.position[i][1]) this.generateApple();
        }
    }
}

class Display {

    constructor(size, pixelSize, speed) {

        this.score = 0;
        this.size = size;
        this.pixelSize = pixelSize;

        this.divGrid = createArray(size, size);
        this.genDivs();
        this.snake = new Snake(this, speed);
        this.apple = new Apple(this);


    }

    //genero i div (html) e gli assegno posizione, colore, grandezza
    genDivs() {

        for (var i = 0; i < this.size; i++) {

            for (var j = 0; j < this.size; j++) {

                ctx = canvas.getContext("2d");
                ctx.fillStyle = "#f5f5f5";
                ctx.fillRect(j * (this.pixelSize), i * (this.pixelSize), this.pixelSize, this.pixelSize);
            }
        }
    }

    getDistances() {

        this.appleDistance = createArray(8);
        this.borderDistance = createArray(8);
        this.snakeDistance = createArray(8);

        var headX = this.snake.position[0][0];
        var headY = this.snake.position[0][1];
        var appleX = this.apple.position[0];
        var appleY = this.apple.position[1];
        var snakeX;
        var snakeY;
        var borderTop = -1;
        var borderLeft = -1;
        var borderRight = this.size;
        var borderBot = this.size;

        //distanza dalla mela;
        for (var i = 0; i < this.size; i++) {

            if (headX + i == appleX && headY == appleY) this.appleDistance[2] = Math.pow(this.size - i, variables.inputImportance.apple); // right
            if (headX - i == appleX && headY == appleY) this.appleDistance[6] = Math.pow(this.size - i, variables.inputImportance.apple); // left
            if (headX == appleX && headY + i == appleY) this.appleDistance[4] = Math.pow(this.size - i, variables.inputImportance.apple); // down
            if (headX == appleX && headY - i == appleY) this.appleDistance[0] = Math.pow(this.size - i, variables.inputImportance.apple); // up
        }

        this.appleDistance[3] = Math.min(this.appleDistance[2], this.appleDistance[4]) * Math.sqrt(2); // down right
        this.appleDistance[5] = Math.min(this.appleDistance[6], this.appleDistance[4]) * Math.sqrt(2); // down left
        this.appleDistance[1] = Math.min(this.appleDistance[2], this.appleDistance[0]) * Math.sqrt(2); // up right
        this.appleDistance[7] = Math.min(this.appleDistance[6], this.appleDistance[0]) * Math.sqrt(2); // up left          

        
        

        //distanza dal corpo del serpente
        // (parto dalla fine così prendo la parte effettivamente più vicina)
        for (var j = this.snake.position.length - 1; j > 0; j--) {

            for (var i = 0; i < this.size; i++) {

                snakeX = this.snake.position[j][0];
                snakeY = this.snake.position[j][1];

                if (headX + i == snakeX && headY == snakeY) this.snakeDistance[2] = Math.pow(this.size - i, variables.inputImportance.skaneBody); // right
                if (headX - i == snakeX && headY == snakeY) this.snakeDistance[6] = Math.pow(this.size - i, variables.inputImportance.skaneBody); // left
                if (headX == snakeX && headY + i == snakeY) this.snakeDistance[4] = Math.pow(this.size - i, variables.inputImportance.skaneBody); // down
                if (headX == snakeX && headY - i == snakeY) this.snakeDistance[0] = Math.pow(this.size - i, variables.inputImportance.skaneBody); // up        

            }
        }

        this.snakeDistance[3] = Math.min(this.borderDistance[2], this.borderDistance[4]) * Math.sqrt(2); // down right
        this.snakeDistance[5] = Math.min(this.borderDistance[6], this.borderDistance[4]) * Math.sqrt(2); // down left
        this.snakeDistance[1] = Math.min(this.borderDistance[2], this.borderDistance[0]) * Math.sqrt(2); // up right
        this.snakeDistance[7] = Math.min(this.borderDistance[6], this.borderDistance[0]) * Math.sqrt(2); // up left  

        //distanza da bordo 
        for (var i = 0; i < this.size; i++) {

            if (headX + i == borderRight) this.borderDistance[2] = Math.pow(this.size - i, variables.inputImportance.border); // right
            if (headX - i == borderLeft) this.borderDistance[6] = Math.pow(this.size - i, variables.inputImportance.border); // left
            if (headY + i == borderBot) this.borderDistance[4] = Math.pow(this.size - i, variables.inputImportance.border); // down
            if (headY - i == borderTop) this.borderDistance[0] = Math.pow(this.size - i, variables.inputImportance.border); // up

        }

        this.borderDistance[3] = Math.min(this.borderDistance[2], this.borderDistance[4]) * Math.sqrt(2); // down right
        this.borderDistance[5] = Math.min(this.borderDistance[6], this.borderDistance[4]) * Math.sqrt(2); // down left
        this.borderDistance[1] = Math.min(this.borderDistance[2], this.borderDistance[0]) * Math.sqrt(2); // up right
        this.borderDistance[7] = Math.min(this.borderDistance[6], this.borderDistance[0]) * Math.sqrt(2); // up left          

        
    }
}





//impacchetto gli inpur per mandargl alla rete neurale
function getInputs() {

    var inputs = display.appleDistance.concat(display.snakeDistance, display.borderDistance);

    for (var i = 0; i < inputs.length; i++) {

        if (i % 2 == 0) inputs[i] = (inputs[i] - 17.5) / 17.5;
        else inputs[i] = (inputs[i] - 24.74873) / 24.74873;
        if (isNaN(inputs[i])) inputs[i] = 0;
    }
    //console.log(inputs);
   // console.log(inputs);
    return inputs;
}


//funzione chiamata ogni volta che il serpente si muove
function refreshDisplay() {

    //visualizeNN();
    //console.log(snakeBrain);
    //console.log(display.appleDistance[2]);
    //controllo se il serpente si scontra con se stesso
    for (var i = 1; i < display.snake.position.length; i++) {

        if (display.snake.position[i].equals(display.snake.position[0])) {
            gameOver();
            return;
        }
    }

    //controllo se il serpente si scontra con un lato della mappa
    for (var i = 0; i < display.divGrid.length; i++) {

        if ([-1, i].equals(display.snake.position[0])) { //top
            gameOver();
            return;
        }
        if ([i, -1].equals(display.snake.position[0])) { //left
            gameOver();
            return;
        }
        if ([display.size, i].equals(display.snake.position[0])) { //bot
            gameOver();
            return;
        }
        if ([i, display.size].equals(display.snake.position[0])) { //right
            gameOver();
            return;
        }
    }

    //controllo se il serpente ha mangiato la mela
    if (display.apple.position.equals(display.snake.position[0])) {
        display.snake.position.push(display.snake.position[display.snake.position.length - 1]);
        display.apple = new Apple(display);
        display.score += 100;
    }

    //grafica  con canvas
    for (var i = 0; i < 35; i++) {

        for (var j = 0; j < 35; j++) {


            if (display.snake.position[0][0] == j &&
                display.snake.position[0][1] == i) {

                ctx = canvas.getContext("2d");
                ctx.fillStyle = "#20a32f";
                ctx.fillRect(j * 30, i * 30, 30, 30);
            }
            if (display.snake.position[display.snake.position.length - 1][0] == j &&
                display.snake.position[display.snake.position.length - 1][1] == i) {

                ctx = canvas.getContext("2d");
                ctx.fillStyle = "#f5f5f5";
                ctx.fillRect(j * 30, i * 30, 30, 30);
            }
            if (display.apple.position[0] == j &&
                display.apple.position[1] == i) {

                ctx = canvas.getContext("2d");
                ctx.fillStyle = "#e21212";
                ctx.fillRect(j * 30, i * 30, 30, 30);
            }
        }
    }
    //faccio comparire / scomparire il serpente
    //display.divGrid[display.snake.position[display.snake.position.length - 1][0]][display.snake.position[display.snake.position.length - 1][1]].style.background = "#e2e2e2";
    //display.divGrid[display.snake.position[0][0]][display.snake.position[0][1]].style.background = "#20a32f";

    //faccio comparire la mela 
    //display.divGrid[display.apple.position[0]][display.apple.position[1]].style.background = "#e21212";


    //se il serpente si avvicina alla mela
    if( display.snake.previousDirection == "right" && display.apple.position[0] > display.snake.position[0][0]
        || display.snake.previousDirection == "left" && display.apple.position[0] < display.snake.position[0][0]
        || display.snake.previousDirection == "bot" && display.apple.position[1] > display.snake.position[0][1]
        || display.snake.previousDirection == "top" && display.apple.position[1]  < display.snake.position[0][1]) 
        display.score += 4;
    else display.score -= 2;
    display.score++;


    display.getDistances();

}



var canvas;
var ctx;
var aWrapper;
var resizer;
var display;
var called;
var snakeBrain;
var direction = "bot";
var doSendData = true;
var gameInterval;
var manualControl = false;
var isLooping;

export function createSnake() {

    //check se il serpente va in loop (non invio i dati)
    isLooping = window.setTimeout(() => {

        if (!called){

            clearInterval(gameInterval);
            clearTimeout(isLooping);
            ctx.clearRect(0, 0, 1300, 1300);
            display.genDivs();
            initialize();
        }
    
    },10000);

    //impostazioni canvas
    called = false;
    canvas = document.getElementById("myCanvas");
    ctx = canvas.getContext("2d");
    aWrapper = document.getElementById("aWrapper");

    //resizeCanvas();
    display = new Display(35, 30, 2);
    // resizer = window.addEventListener("resize", resizeCanvas, false);
    // richiamo le funzioni per aggiornare lo snake
    gameInterval = window.setInterval(function () {

        if (direction === "top") display.snake.goTop(direction);
        if (direction === "bot") display.snake.goBot(direction);
        if (direction === "left") display.snake.goLeft(direction);
        if (direction === "right") display.snake.goRight(direction);

        if (!manualControl) {
            //ottengo la rete neurale
            snakeBrain = getNeuralNetwork(getInputs());
            var output = snakeBrain.output;
            if (output == 1) direction = "top";
            if (output == 2) direction = "right";
            if (output == 3) direction = "bot";
            if (output == 4) direction = "left";
        }

        //visualizzazione neuroni attivati (con output maggiore)
        visualizeOutputs();

       
        if (called) visualizeWeights();

    }, 1 / display.snake.speed);

    
}

//cambio direzione
document.addEventListener('keydown', (e) => {

    if (e.keyCode === 38 /* up */ || e.keyCode === 87 /* w */ )
        direction = "top";
    if (e.keyCode === 39 /* right */ || e.keyCode === 68 /* d */ )
        direction = "right";
    if (e.keyCode === 40 /* down */ || e.keyCode === 83 /* s */ )
        direction = "bot";
    if (e.keyCode === 37 /* left */ || e.keyCode === 65 /* a */ )
        direction = "left";
});


export function noGeneration() {
    doSendData = false;
}


function gameOver() {

    //console.log(snakeBrain);
    if (!called) {

        called = true;
        //se il tasto on non è attivo
        if (!on) {

            //mando i dati al server
            if (doSendData) sendData(snakeBrain);

            clearInterval(gameInterval);
            clearTimeout(isLooping);

            //handlo il ripremere il tasto on
            var changedToOn = setInterval(() => {

                if (on) {
                    ctx.clearRect(0, 0, 1300, 1300); // resetto il canvas
                    initialize(); //reinizializzo tutto
                    clearInterval(changedToOn);
                }
            }, 500);

        } else {

            //console.log("game over" + Date.now());
            //console.log("score: " + display.score);
            
            if (doSendData) sendData(snakeBrain);
            //window.removeEventListener("resize", resizer);
            clearInterval(gameInterval);
            clearTimeout(isLooping);
            //location.reload();

            ctx.clearRect(0, 0, 1300, 1300);
            display.genDivs();
            initialize();
        }
    }

}

//funzione per mandare i dati al server -> db
function sendData(snakeBrain) {

    var data = {
        "score": display.score,
        "weights": snakeBrain.weights,
        "biases": snakeBrain.biases,
    }

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "data");
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.send(JSON.stringify(data));

}


function visualizeOutputs(){

    Array.from(document.getElementsByClassName("neuron")).forEach(element => {
        element.style.backgroundColor = "transparent";
    });

    snakeBrain.layers.forEach((layer, i) => {
        
        var j = layer.outputs.reduce((iMax, x, k, arr) => x > arr[iMax] ? k : iMax, 0);
                  
        document.getElementById("layer" + i + "neuron" + j).style.backgroundColor = "green";   
        
    });
}

function visualizeWeights(){

    console.log(snakeBrain);
    snakeBrain.layers.forEach((layer, i) => {

        if (i == 0) return; // visto che foreach è una funzione return = continue
        layer.neurons.forEach((neuron, j) => {

            neuron.weights.forEach((weight, k) => {

                document.getElementById("line" + "layer" + (i - 1) + "neuron" + k + "layer" + i + "neuron" + j).style.strokeWidth = (0.8 / ( 1 + Math.pow(Math.E, -( 5 * weight)))) + "px";
                
            });
        });
    });
}


//funzioni prese online
//funzione per creare facilmente array
function createArray(length) {
    var arr = new Array(length || 0),
        i = length;

    if (arguments.length > 1) {
        var args = Array.prototype.slice.call(arguments, 1);
        while (i--) arr[length - 1 - i] = createArray.apply(this, args);
    }

    return arr;
}

//funzione per comparare 2 array
if (Array.prototype.equals)
    console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
Array.prototype.equals = function (array) {
    if (!array)
        return false;

    if (this.length != array.length)
        return false;

    for (var i = 0, l = this.length; i < l; i++) {
        if (this[i] instanceof Array && array[i] instanceof Array) {
            if (!this[i].equals(array[i]))
                return false;
        } else if (this[i] != array[i]) {
            return false;
        }
    }
    return true;
}
Object.defineProperty(Array.prototype, "equals", {
    enumerable: false
});

//funzione per aggiustare la size del canvas !?
function setCanvasScalingFactor() {
    return window.devicePixelRatio || 1;
}

function resizeCanvas() {
    var pixelRatio = setCanvasScalingFactor();

    if (window.innerHeight > window.innerWidth) {
        var width = Math.round(1.0 * window.innerWidth);
    } else {
        var width = Math.round(1.0 * window.innerHeight);
    }

    var height = width;

    aWrapper.style.width = width + "px";
    aWrapper.style.height = height + "px";
    canvas.width = width * pixelRatio;
    canvas.height = height * pixelRatio;
}