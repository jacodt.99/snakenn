
class InputNeuron{

    constructor(input,bias){

        this.output = /*5 *  Math.pow(input, 1/3)  + 0.5*/ /*= input + bias;*/  input;
/*         if (this.output > 0) this.output = this.output;
        else this.output = 0; */

        // sigmoid https://www.desmos.com/calculator/umvakihok5

        //avrebbe senso far si che a seconda della presenza o meno della mela, per esempio, campi il numero di neurono input,tipo se non la vede in nessuna
        //direzione ci saranno 16 neuroni in input,

        //il problema è la normalizzazione, visto che l'input varia poco al variare della posizione, anche l'output
    }
}


class Neuron{

    constructor(inputs, weights, bias){

        this.weights = weights;
        this.bias = bias;
        this.inputs = inputs;

        this.activationFunction();
    }

    sumFunction(){

        var sum = 0;

        for (var i = 0; i < this.inputs.length; i++){
            sum += (this.inputs[i] * this.weights[i]);
        }
        return (sum + this.bias);
    }

    activationFunction(){
        this.output = this.sumFunction();//1 / (1 + Math.pow(Math.E, -(this.sumFunction())));// 1000 / (1 + Math.pow(Math.E, -((this.sumFunction())  * 0.004)));
        if (this.sumFunction() < 0) this.output = 0; 

        //console.log(this.output);
    }
}

class Layer{

    constructor(inputs, weights, biasesss){

        this.biases = biasesss;
        this.inputs = inputs;
        this.weights = weights;
        this.neurons = new Array();
        this.outputs = new Array();

        
    
        this.createNeurons();
        this.calcolateOutputs();
    }

    createNeurons(){
        
        
        if (this.weights == undefined){
            
            for (var i = 0; i < 24; i++){
        
                this.neurons[i] = new InputNeuron(this.inputs[i], this.biases[i]);
            }
        }
        else{


            for (var i = 0; i < this.weights.length; i++){
            
                this.neurons[i] = new Neuron(this.inputs,this.weights[i],this.biases[i]);
            }
        }
    }

    calcolateOutputs(){

        for (var i = 0; i < this.neurons.length; i++){

            this.outputs[i] = this.neurons[i].output;
        }
    }
}

class NeuralNetwork{

    constructor(inputs,weights,biasess){

        this.inputs = inputs;
        this.weights = weights;
        this.biases = biasess;
        this.layers = [
            new Layer(this.inputs,undefined, this.biases[0])
        ];
        

        this.output;
        this.createNetwork();
    }

    createNetwork(){

        //creo un gli altri 3 layers
        this.layers[1] = new Layer(this.layers[0].outputs, this.weights[1], this.biases[1]);
        this.layers[2] = new Layer(this.layers[1].outputs, this.weights[2], this.biases[2]);
        this.layers[3] = new Layer(this.layers[2].outputs, this.weights[3], this.biases[3]);
        this.layers[4] = new Layer(this.layers[3].outputs, this.weights[4], this.biases[4]);
        this.layers[5] = new Layer(this.layers[4].outputs, this.weights[5], this.biases[5]);
	 
        //prendo gli output

        var outputs = new Array();         
        outputs = this.layers[5].outputs; 
        var max = Math.max(outputs[0], outputs[1], outputs[2], outputs[3]);
        //console.log(outputs);
        if (outputs[0] == max) this.output = 1;
        if (outputs[1] == max) this.output = 2;
        if (outputs[2] == max) this.output = 3;
        if (outputs[3] == max) this.output = 4;


    }
}

var brain;

export function setBrain(brainData){

    brain = brainData;
}

export function getNeuralNetwork(inputs) {

    return new NeuralNetwork(inputs,brain.weights,brain.biases);
}





//funzione per creare facilmente array
function createArray(length) {
    var arr = new Array(length || 0),
        i = length;

    if (arguments.length > 1) {
        var args = Array.prototype.slice.call(arguments, 1);
        while(i--) arr[length-1 - i] = createArray.apply(this, args);
    }

    return arr;
}