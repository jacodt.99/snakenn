import {
    noGeneration
} from './snake.js';
import {
    variables
} from './configuration.js';

var minB;
var maxB;
var minW;
var maxW;
var percentages;

export function muteBrain(data) {

    minB = variables.biasesRange[0];
    maxB = variables.biasesRange[1];
    minW = variables.weightsRange[0];
    maxW = variables.weightsRange[1];
    percentages = variables.mutatePerc;
    var newBiases;
    var newWeights;

    var bestBrains = data;

    if (!(bestBrains == "first")) {

        newBiases = mutateBiases(getRanomBiases(), bestBrains);
        newWeights = mutateWeights(getRandomWeights(), bestBrains);
    } else {
        //console.log("first generation <3");
        newBiases = getRanomBiases();
        newWeights = getRandomWeights();
    }

    return {
        'biases': newBiases,
        'weights': newWeights
    }
}


function mutateBiases(biases, bestBrains) {

    var random;

    for (var j = 0; j < bestBrains.length; j++) {

        for (var i = 0; i < 24; i++) {
            random = Math.random() * 100;
            if (random < percentages[j] && random > percentages[j + 1]) biases[0][i] = bestBrains[j].biases[0][i];
            //else console.log(i);
        }
        for (var i = 0; i < 8; i++) {
            random = Math.random() * 100;
            if (random < percentages[j] && random > percentages[j + 1]) biases[1][i] = bestBrains[j].biases[1][i];
        }
        for (var i = 0; i < 8; i++) {
            random = Math.random() * 100;
            if (random < percentages[j] && random > percentages[j + 1]) biases[2][i] = bestBrains[j].biases[2][i];
        }
        for (var i = 0; i < 8; i++) {
            random = Math.random() * 100;
            if (random < percentages[j] && random > percentages[j + 1]) biases[3][i] = bestBrains[j].biases[3][i];
        }
        for (var i = 0; i < 8; i++) {
            random = Math.random() * 100;
            if (random < percentages[j] && random > percentages[j + 1]) biases[4][i] = bestBrains[j].biases[4][i];
        }
        for (var i = 0; i < 4; i++) {
            random = Math.random() * 100;
            if (random < percentages[j] && random > percentages[j + 1]) biases[5][i] = bestBrains[j].biases[5][i];
        }
    }
    return biases;
}

function mutateWeights(weights, bestBrains){

    var portion = 0;
    var randomBrain;
    var k = 0;

    for (var i = 0; i < 8; i++) {
        for (var j = 0; j < 24; j++) {
            k++
            if (k >= portion) {  
                randomBrain = Math.floor(Math.random() * 19);
                portion = Math.floor(Math.random() * variables.portionMaxSize);
                k = 0;
            }

            // possibilità di mutamento
            if (Math.random() * 100 < variables.randomMutate) continue;

            weights[1][i][j] = bestBrains[randomBrain].weights[1][i][j];
        }
    }

    for (var i = 0; i < 8; i++) {
        for (var j = 0; j < 8; j++) {
            k++
            if (k >= portion) {  
                randomBrain = Math.floor(Math.random() * 19);
                portion = Math.floor(Math.random() * variables.portionMaxSize);
                k = 0;
            }

            // possibilità di mutamento
            if (Math.random() * 100 < variables.randomMutate) continue;
      
            weights[2][i][j] = bestBrains[randomBrain].weights[2][i][j];
        }
    }
    for (var i = 0; i < 8; i++) {
        for (var j = 0; j < 8; j++) {
            k++
            if (k >= portion) {  
                randomBrain = Math.floor(Math.random() * 19);
                portion = Math.floor(Math.random() * variables.portionMaxSize);
                k = 0;
            }

            // possibilità di mutamento
            if (Math.random() * 100 < variables.randomMutate) continue;
       
            weights[3][i][j] = bestBrains[randomBrain].weights[3][i][j];
        }
    }
    for (var i = 0; i < 8; i++) {
        for (var j = 0; j < 8; j++) {
            k++
            if (k >= portion) {  
                randomBrain = Math.floor(Math.random() * 19);
                portion = Math.floor(Math.random() * variables.portionMaxSize);
                k = 0;
            }

            // possibilità di mutamento
            if (Math.random() * 100 < variables.randomMutate) continue;
          
            weights[4][i][j] = bestBrains[randomBrain].weights[4][i][j];
        }
    }
    for (var i = 0; i < 4; i++) {
        for (var j = 0; j < 8; j++) {
            k++
            if (k >= portion) {  
                randomBrain = Math.floor(Math.random() * 19);
                portion = Math.floor(Math.random() * variables.portionMaxSize);
                k = 0;
            }

            // possibilità di mutamento
            if (Math.random() * 100 < variables.randomMutate) continue;
        
            weights[5][i][j] = bestBrains[randomBrain].weights[5][i][j];
        }
    }

    return weights;
} 


function mutateWeightss(weights, bestBrains) {

    var random;
    console.log(weights);
    console.log(bestBrains);

    for (var j = 0; j < bestBrains.length; j++) {

        for (var i = 0; i < 8; i++) {
            for (var k = 0; k < 24; k++) {
                random = Math.random() * 100;
                if (random < percentages[j] && random > percentages[j + 1]) weights[1][i][k] = bestBrains[j].weights[1][i][k];
            }
        }
        for (var i = 0; i < 8; i++) {
            for (var k = 0; k < 8; k++) {
                random = Math.random() * 100;
                if (random < percentages[j] && random > percentages[j + 1]) weights[2][i][k] = bestBrains[j].weights[2][i][k];
            }
        }
        for (var i = 0; i < 8; i++) {
            for (var k = 0; k < 8; k++) {
                random = Math.random() * 100;
                if (random < percentages[j] && random > percentages[j + 1]) weights[3][i][k] = bestBrains[j].weights[3][i][k];
            }
        }
        for (var i = 0; i < 8; i++) {
            for (var k = 0; k < 8; k++) {
                random = Math.random() * 100;
                if (random < percentages[j] && random > percentages[j + 1]) weights[4][i][k] = bestBrains[j].weights[4][i][k];
            }
        }
        for (var i = 0; i < 4; i++) {
            for (var k = 0; k < 8; k++) {
                random = Math.random() * 100;
                if (random < percentages[j] && random > percentages[j + 1]) weights[5][i][k] = bestBrains[j].weights[5][i][k];
            }
        }
    }

    return weights;
}



function getRanomBiases() {

    var biases = [
        createArray(24),
        createArray(8),
        createArray(8),
        createArray(8),
        createArray(8),
        createArray(4)
    ];

    for (var i = 0; i < 24; i++) {
        biases[0][i] = (gaussianRand() * (maxB - minB) + minB);
    }
    for (var i = 0; i < 8; i++) {
        biases[1][i] = (gaussianRand() * (maxB - minB) + minB);
    }
    for (var i = 0; i < 8; i++) {
        biases[2][i] = (gaussianRand() * (maxB - minB) + minB);
    }
    for (var i = 0; i < 8; i++) {
        biases[3][i] = (gaussianRand() * (maxB - minB) + minB);
    }
    for (var i = 0; i < 8; i++) {
        biases[4][i] = (gaussianRand() * (maxB - minB) + minB);
    }
    for (var i = 0; i < 4; i++) {
        biases[5][i] = (gaussianRand() * (maxB - minB) + minB);
    }

    return biases;

}

function getRandomWeights() {

    //console.log(gaussianRand())
    var weights = [
        createArray(0, 0),
        createArray(8, 24),
        createArray(8, 8),
        createArray(8, 8),
        createArray(8, 8),
        createArray(4, 8)
    ];

    for (var i = 0; i < 8; i++) {
        for (var k = 0; k < 24; k++) {
            weights[1][i][k] = (gaussianRand() * (maxW - minW) + minW);
        }
    }
    for (var i = 0; i < 8; i++) {
        for (var k = 0; k < 8; k++) {
            weights[2][i][k] = (gaussianRand() * (maxW - minW) + minW);
        }
    }
    for (var i = 0; i < 8; i++) {
        for (var j = 0; j < 8; j++) {
            weights[3][i][j] = (gaussianRand() * (maxW - minW) + minW);
        }
    }
    for (var i = 0; i < 8; i++) {
        for (var j = 0; j < 8; j++) {
            weights[4][i][j] = (gaussianRand() * (maxW - minW) + minW);
        }
    }
    for (var i = 0; i < 4; i++) {
        for (var j = 0; j < 8; j++) {
            weights[5][i][j] = (gaussianRand() * (maxW - minW) + minW);
        }
    }

    return weights;
}


function createArray(length) {
    var arr = new Array(length || 0),
        i = length;

    if (arguments.length > 1) {
        var args = Array.prototype.slice.call(arguments, 1);
        while (i--) arr[length - 1 - i] = createArray.apply(this, args);
    }

    return arr;
}

function gaussianRand() {
    var rand = 0;
  
    for (var i = 0; i < 6; i += 1) {
      rand += Math.random();
    }
  
    return rand / 6;
  }