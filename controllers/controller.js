var bodyParser = require('body-parser');
var mongoose = require("mongoose");

var urlencodedParser = bodyParser.urlencoded({
  extended: false
})



//mi connetto ad db con mongoose
mongoose.connect("mongodb://localhost/snakeNN", {
  useNewUrlParser: true
});
console.log("connected to db!");

//create a schema
var snakeNNSchema = new mongoose.Schema({

  score: Number,
  weights: [
    [
      [Number]
    ]
  ],
  biases: [
    [Number]
  ],
  generation: Number
})

var NNData = mongoose.model('snakeNN', snakeNNSchema, 'NNData');

module.exports = function (app) {

  //loggo gli ip delle richieste mandate
  app.get("*", function (req, res, next) {
    var ip = (req.headers['x-forwarded-for'] || req.connection.remoteAddress).substring(7);
    console.log("request " + ip);
    next();
  });
  //handle get request
  app.get(["/", "/home"], function (req, res) {

    res.status(200);
    res.sendFile("C:\\Users\\jacopo\\Desktop\\node_snake\\views\\snake.html");
  });
  app.get("/search", function (req, res) {


    res.status(200);
    res.sendFile("C:\\Users\\jacopo\\Desktop\\node_test\\views\\search.html");
  });
  //mando i primi 5 snake dell'ultima generazione
  app.get('/getFirstFive', function (req, res) {

    console.log("requested");
    NNData.findOne({}, function (err, data) {

      var generation;
      if (data == null) {
        res.status(200);
        res.send({
          "FS": true
        });
        res.end();
        return;
      }
      generation = data.generation - 1;
      if (data.generation == 1) {
        generation = 1;

        if (data != null) res.send({
          "FS": true
        });
        res.end();
        return;
      }

      NNData.find({}, function (err, data) {

        if (err) return res.status(500).send(err);
        res.status(200);
        res.send(data);
        res.end();
      }).where('generation').equals(generation).sort({
        'score': -1
      }).limit(20);
    }).sort({
      'generation': -1
    });
  });
  app.get('/info', function (req, res) {

  });

  app.get('/getStats', function (req, res) {

    var lastGeneration;

    NNData.findOne({}, function (err, data) {

      if (data != null) lastGeneration = data.generation;
      callback(lastGeneration);

    }).sort({
      'generation': -1
    });

    function callback(lastGeneration) {

      var dataToSend = [];
      var counter = 0;
      var counterD = 0;
      var sum = 0;

      for (var i = 1; i < lastGeneration + 1; i++) {

        NNData.find({}, function (err, data) {




          for (var j = 0; j < data.length; j++) {

            sum += data[j].score;
            counterD++;
          }

          if (err) return res.status(500).send(err);
          dataToSend.push({
            'generation': data[0].generation,
            'bestScore': data[0].score,
            'avgScore': (sum / counterD)
          });
          sum = 0;
          counterD = 0;
          finished();

        }).where('generation').equals(i).sort({
          'score': -1
        });
      }

      function finished() {

        counterD = 0;
        counter++;
        if (counter == lastGeneration) {
          res.send(dataToSend);
          res.status(200);
          res.end();
        }
      }

    }
  });
  app.get('/info', function (req, res) {

  });

  //handle tutte le route non gestite
  app.get('*', function (req, res) {

    res.status(404);
    res.sendFile("C:\\Users\\jacopo\\Desktop\\node_test\\views\\404.html");
    res.end();
  });




  //handle post requests

  // posto il serpente ricevuto nel db
  app.post("/data", function (req, res) {

    NNData.find({}, function (err, data) {
      var filledGen = true;
      var counter = 0;

      //verifico se necessario cambiare generazione

      if (data[0] !== undefined) {
        for (var i = 1; i < data.length; i++) {

          if (data[0].generation == data[i].generation) counter++
        }

        if (counter >= 39) {
          req.body['generation'] = data[0].generation + 1;
        } else req.body['generation'] = data[0].generation;
      } else req.body['generation'] = 1;

      const newNNData = new NNData(req.body);

      newNNData.save(err => {

        if (err) return res.status(500).send(err);
        res.status(200);
        res.end();
        return;

      });
    }).sort({
      'generation': -1
    }).limit(40);
  });


  //handle delete requests
  app.delete("/gens", function (req, res) {

    if (req.body["pass"] == "ciao") deleteCollection();

    function deleteCollection() {
      mongoose.connection.db.dropCollection('NNData', function (err, result) {

        console.log("deletes collection");
      });
    }
    res.status(200);
    res.end();
  });
};